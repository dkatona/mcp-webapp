# Basic setup

Create/copy project code to `/webapp`

## Customize the project

### Optional
Change db name, ports, etc in `.env`
Change the project name in `.devcontainer/devcontainer.json`.
